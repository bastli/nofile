#include "cairoif.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>

int cairoif_socket_open(const char *name, const char *port){
	struct addrinfo hints;
	struct addrinfo *result, *rp;
	int s;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;		/* Allow IPv4 or IPv6 */
	hints.ai_socktype = SOCK_DGRAM;		/* Datagram socket */
	hints.ai_flags = AI_PASSIVE;   		/* For wildcard IP address */
	hints.ai_protocol = IPPROTO_UDP;	/* Only UDP */
	hints.ai_canonname = NULL;
	hints.ai_addr = NULL;
	hints.ai_next = NULL;

	s = getaddrinfo(name, port, &hints, &result);
	if (s != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
		return -1;
	}
	
	int sock;
	for (rp = result; rp != NULL; rp = rp->ai_next) {
		sock = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
		if (sock == -1)
			continue;
		if (connect(sock, rp->ai_addr, rp->ai_addrlen) != -1)
			break;
		close(sock);
		sock=-1;
	}
	freeaddrinfo(result);
	return sock;
}

void cairoif_socket_close(int socket){
	close(socket);
}

void cairoif_socket_send(int socket, unsigned char *data, unsigned int length){
	write(socket, data, length);
}

uint8_t* cairoif_get_buffer(cairo_t *cr, int x0, int y0, enum TRANSFORM t){
	cairo_surface_t *surf=cairo_get_group_target(cr);
	if(surf==NULL){
		fprintf(stderr, "No surface associated with cairo context\n");
		return NULL;
	}
	cairo_surface_flush(surf);
	static uint8_t ret[NOFILE_BUFSIZE];
	int stride=cairo_image_surface_get_stride(surf);
	uint8_t *dat=cairo_image_surface_get_data(surf);
	int x,y;
	int w=cairo_image_surface_get_width(surf), h=cairo_image_surface_get_height(surf);
	int id=0;
	uint8_t *destptr;
	if(t == ROTATE_180){
		destptr = &ret[NOFILE_BUFSIZE];
	}
	else{
		destptr = ret-1;
	}
	uint8_t *srcptr=dat;
	for(y=y0; y<NOFILE_HEIGHT+y0; y++){
		srcptr=&dat[y*stride];
		for(x=x0; x<NOFILE_WIDTH+x0; x++){
			if((id++)%8==0){
				if(t == ROTATE_180){
					destptr--;
				}
				else{
					destptr++;
				}
				*(destptr)=0;
			}
			uint8_t val;
			if(x<0 || x>=w || y<0 || y>=h){
				val=0;
			}
			else{
				val=((srcptr[x/8]>>(x%8))&1);
			}
			if(t == ROTATE_180){
				*destptr=(*destptr>>1)|(val<<7);
			}
			else{
				*destptr=(*destptr<<1)|(val&1);
			}
		}
	}

	return ret;
}

int cairoif_send_transform(cairo_t *c, int s, int offs_x, int offs_y, enum TRANSFORM t){
	uint8_t *data=cairoif_get_buffer(c, offs_x, offs_y, t);
	if(data==NULL){
		return -1;
	}
	
	#ifdef DEBUG_WRITE
	FILE *f=fopen("/tmp/derp.pbm", "w");
	fprintf(f, "P4\n112 7\n");
	fwrite(data, sizeof(char), NOFILE_BUFSIZE, f);
	fclose(f);
	#endif
	cairoif_socket_send(s, data, NOFILE_BUFSIZE);
	return 0;
}

int cairoif_send(cairo_t *c, int s, int offs_x, int offs_y){
	return cairoif_send_transform(c,s,offs_x,offs_y,NORMAL);
}

cairo_t* cairoif_init(int width, int height){
	cairo_surface_t* surf=cairo_image_surface_create (CAIRO_FORMAT_A1,width,height);
	cairo_t *res=cairo_create(surf);
	cairo_surface_destroy(surf);
	return res;
}

void cairoif_destroy(cairo_t* c){
	cairo_destroy(c);
}

