import std.stdio;
import std.socket;
import std.json;
import std.format;
import std.algorithm;

void main(string[] args){
	Address address = parseAddress(args[1], args[2]);
	
	Socket s=new TcpSocket();
	s.bind(address);
	scope(exit){
		s.close();
	}
	while(true){
		s.listen(0);
		auto client=s.accept();
		scope(exit){
			client.close();
		}
		string response(int code, in char[] code_s, in char[] response){
			return format("HTTP/1.1 %d %s\r\nAccess-Control-Allow-Origin: *\r\nAccess-Control-Allow-Headers: Content-Type,Content-Length\r\nAccess-Control-Allow-Methods: POST,GET,OPTIONS\r\nContent-Type: application/json\r\nContent-Length: %d\r\n\r\n%s\r\n",
				code, code_s, response.length+2, response);
		}
		void respond(int code, string code_s, string txt){
			string res=response(code,code_s,txt);
			client.send(res);
		}
		try{
			char[4096] buf;
			auto resp=buf[0..client.receive(buf)];
			if(resp.length==0){
				continue;
			}
			auto payload=find(cast(string)resp, "\r\n\r\n");
			if(payload.length>4){
				auto jv=parseJSON(payload[4..$], 2);
				auto str=jv["text"].str;
				if(str.length>512){
					throw new Exception("Text too long");
				}
				writeln(str);
				stdout.flush();
			}
		}
		catch(Exception e){
			respond(400, "U NOOB", format(`{"error":"%s"}`, e.msg));
		}
		respond(200, "OK", `{"success":true}`);
	}
}
