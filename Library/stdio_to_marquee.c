#include "cairoif.h"
#include <pango/pangocairo.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

int step=1;
unsigned long delay=1000*30;

typedef void(*mode)(int, PangoLayout*, cairo_t*, char*);

static int draw(PangoLayout *layout, cairo_t *c, char *text){
	cairo_save(c);
	cairo_set_source_rgba (c, 1, 1, 1, 0);
	cairo_paint(c);
	
	cairo_set_source_rgba (c, 1, 1, 1, 1);
	cairo_move_to(c, NOFILE_WIDTH, -1);
	
	pango_layout_set_text (layout, text, -1);
	pango_cairo_show_layout (c, layout);
	PangoRectangle pr;
	pango_layout_get_pixel_extents(layout, &pr, NULL);
	cairo_restore(c);
	return pr.width;
}

static void marquee(int sock, PangoLayout *layout, cairo_t *c, char *text){
	int width=draw(layout, c, text);
	int x;
	for(x=0; x<width+2*NOFILE_WIDTH; x+=step){
		cairoif_send(c, sock, x,0);
		usleep(delay);
	}
}

static void display(int sock, PangoLayout *layout, cairo_t *c, char *text){
	int width=draw(layout, c, text);
	cairoif_send(c, sock, (width/NOFILE_WIDTH)*NOFILE_WIDTH+NOFILE_WIDTH, 0);
}

struct ModeEntry{
	char *name;
	mode mode;
};

struct ModeEntry modeEntries[]={
	{"MARQUEE", &marquee},
	{"DISPLAY", &display},
	{NULL, NULL}
};

mode getMode(const char *name){
	struct ModeEntry *m;
	for(m=modeEntries; m->name!=NULL; m++){
		if(strcmp(m->name,name)==0){
			return m->mode;
		}
	}
	return NULL;
}


int main(int argc, char **argv){

	mode current_mode=&marquee;
	if(argc < 3){
		fprintf(stderr, "Usage: %s <Address> <Port> [Delay (µs)] [Step]\n", argv[0]);
	}
	if(argc >= 4){
		sscanf(argv[3], "%ld", &delay);
	}
	if(argc >= 5){
		sscanf(argv[3], "%d", &step);
	}
	int sock=cairoif_socket_open(argv[1], argv[2]);
	if(sock<0){
		return 1;
	}
	
	
	cairo_t *c=cairoif_init(NOFILE_WIDTH*50, NOFILE_HEIGHT);
	cairo_set_operator (c, CAIRO_OPERATOR_SOURCE);
	
	PangoFontDescription *f=pango_font_description_new();
	pango_font_description_set_family(f, "Smallest Pixel-7");
	pango_font_description_set_absolute_size(f, 10*PANGO_SCALE);
	PangoLayout *layout = pango_cairo_create_layout(c);
	pango_layout_set_font_description(layout, f);
	
	#define BUFLEN 512
	char buf[BUFLEN+1];
	size_t ret;
	while((ret=read(STDIN_FILENO, buf, BUFLEN)) > 0){
		char *b,*a=buf,*tmp;
		buf[ret]='\0';
		while(a!=NULL && a[0] != '\0'){
			b=strstr(a,"\n");
			if(b!=NULL){
				*b++='\0';
			}
			if(*a==0x1B){
				printf("Setting mode to %s\n", a+1);
				mode m=getMode(a+1);
				if(m == NULL){
					printf("Mode not found\n");
				}
				else{
					current_mode=m;
				}
			}
			else{
				current_mode(sock, layout, c, a);
			}
			a=b;
		}
	}
	
	g_object_unref (layout);
	pango_font_description_free(f);
	cairoif_destroy(c);
	cairoif_socket_close(sock);
	return 0;
}
