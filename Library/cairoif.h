#include <cairo.h>
#include <stdint.h>

#define NOFILE_WIDTH 112
#define NOFILE_HEIGHT 7
#define NOFILE_LEDCNT (NOFILE_WIDTH*NOFILE_HEIGHT)
#define NOFILE_BUFSIZE (NOFILE_LEDCNT/8)

enum TRANSFORM{
	NORMAL,
	ROTATE_90,
	ROTATE_180,
	ROTATE_270,
	MIRROR_Y,
	MIRROR_X
};

int cairoif_socket_open(const char *name, const char *port);
void cairoif_socket_close(int socket);
uint8_t* cairoif_get_buffer(cairo_t *cr, int x0, int y0, enum TRANSFORM t);
int cairoif_send(cairo_t *c, int s, int offs_x, int offs_y);
int cairoif_send_transform(cairo_t *c, int s, int offs_x, int offs_y, enum TRANSFORM t);
cairo_t* cairoif_init(int width, int height);
void cairoif_destroy(cairo_t* c);
