#include <unistd.h>

static char reverse(char b) {
   b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
   b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
   b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
   return b;
}

int main(int argc, char **argv){
	char buffer[255];
	ssize_t ret=0;
	do{
		ret=read(STDIN_FILENO, buffer, sizeof(buffer));
		unsigned int i=0;
		for(i=0; i<ret; i++){
			buffer[i]=reverse(buffer[i]);
		}
		write(STDOUT_FILENO, buffer, ret);
	}while(ret>0);
	return 0;	
}
