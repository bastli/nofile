No File
=======

This project contains code needed for driving the shift-register
based LED matrix display.

Hardware
--------

The hardware consists of 7 lines LEDs, each line being 112 individual
diodes. Via a BCD to decimal-converter one can select a line, which will
then display whatever is inside the shift-register.
Therefore, the code has to do multiplexing (iterating through the lines
and update the shift-registers with the corresponding content, and doing
this fast enough for the eye not to notice)

Because the shift-registers don't have a latch to withhold the currently
bit-banged line from the output, we have to select an invalid line (e.g. 0)
before updating, such that it wont display our shifting data into the register.
Otherwise, our line will get slurred and bad to look at.

The BCD to decimal-converter has four input-lines (A,B,C,D) and ten outputlines, 
from which only 7 are used. The Outputs are wired a bit strangely to the lines,
so a lookup-table has to be used.

### Lookup-table.

```
┌──┬───┬───┬───┬────────┬──────┐
│A │ B │ C │ D │ Output │ Line │
├──┼───┼───┼───┼────────┼──────┤
│0 │ 0 │ 0 │ 0 │   0    │  ‐   │
├──┼───┼───┼───┼────────┼──────┤
│0 │ 0 │ 0 │ 1 │   1    │  7   │
├──┼───┼───┼───┼────────┼──────┤
│0 │ 0 │ 1 │ 0 │   2    │  3   │
├──┼───┼───┼───┼────────┼──────┤
│0 │ 0 │ 1 │ 1 │   3    │  5   │
├──┼───┼───┼───┼────────┼──────┤
│0 │ 1 │ 0 │ 0 │   4    │  1   │
├──┼───┼───┼───┼────────┼──────┤
│0 │ 1 │ 0 │ 1 │   5    │  6   │
├──┼───┼───┼───┼────────┼──────┤
│0 │ 1 │ 1 │ 0 │   6    │  2   │
├──┼───┼───┼───┼────────┼──────┤
│0 │ 1 │ 1 │ 1 │   7    │  4   │
├──┼───┼───┼───┼────────┼──────┤
│1 │ 0 │ 0 │ 0 │   8    │  ‐   │
├──┼───┼───┼───┼────────┼──────┤
│1 │ 0 │ 0 │ 1 │   9    │  ‐   │
└──┴───┴───┴───┴────────┴──────┘
```

### Connectors

There are 3 relevant connectors

1) DSUB CN2
```
	1. Clock
	3. Data
	5. A
	7. B
	9. C
	11. D
	21. Ground
```
2) CN3
```
	1. 12V
	2. 5V
	3. A Passthrough
	4. B Passthrough
	5. C Passthrough
	6. D Passthrough
```
3) CN5
```
	1. GND 5V
	2. GND 12V
	3. Clock Passthrough
	4. Data Passthrough
```


Software
--------

### Arduino

The Arduino program does two main things:
*	Listens on the serialport with parameters 115200 8N1, and fill an
	off-screen buffer with the data. Once this off-screen buffer is full, 
	it swaps it with the on-screen buffer, which then can be filled and swapped
*	It reads the contents of the on-screen buffer and bit-bangs them into the 
	shift-registers. Every line is then left there for some time, until the next
	one is selected and written into the registers

Note that before bit-banging the line, it has first to be turned off, since we would
see the shifting on the LEDs otherwise.

### bitmirror

This is an utility program to mirror the bits inside every byte of the input-stream.
This is useful for piping a PBM image to the serial tty.

### ftobits

This program reads lines of floating-point values from stdin, and outputs
a bitstream usable for piping to the serial tty.
It does it, such that 0.0 leads to no LEDs being on, and 1.0 to all LEDs in a line being on.

An extra parameter for scaling the floats can be specified on the command line.
This will just be multiplied before converted into LED-counts.

