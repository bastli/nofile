CFLAGS=-Ofast
LDFLAGS=
CC=gcc

.PHONY: utils clean distclean flash

all: utils flash

utils: bitmirror ftobits

flash: Arduino.ino
	arduino --upload $^

clean:
	rm -f *.o

distclean: clean
	rm -f bitmirror ftobits
