#include <unistd.h>
#include <stdio.h>
#include <math.h>
#define LED_CNT 112
#define BYTE_CNT LED_CNT/8
#define min(X,Y) ((X<Y) ? X : Y)

void print_bitstream(int x){
	x=min(x,LED_CNT);
	int i;
	char c=0;
	for(i=0; i<LED_CNT; i++){
		if(i>0 && i%8==0){
			fwrite(&c,1,1,stdout);
		}
		c=(((c&0xFF)>>1)) | ( ( (x-->0)? 1 : 0 ) << 7);
	}
	fflush(stdout);
}

int main(int argc, char **argv){
	float multiplier=1.0;
	if(argc > 1){
		sscanf(argv[1],"%f", &multiplier);
	}
	float value;
	while(scanf("%f",&value)>0){
		value*=multiplier*LED_CNT;
		int x=(unsigned int)value;
		print_bitstream(x);
	}
}
