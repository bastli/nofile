
#include <SPI.h>

#include <EthernetUdp.h>
#include <EthernetClient.h>
//#include <util.h>
#include <Ethernet.h>
#include <EthernetServer.h>


#define LINE_BYTES 56*2/8
#define BUFFER_SIZE 7*LINE_BYTES
#define BIT(BYTE,POS) ((BYTE>>POS)&1)
#define BIT_SET(BYTE, POS) (BYTE |= (1<<POS))
#define BIT_SET_3(BYTE, POS, VAL) (BYTE |= (VAL<<POS))
#define BIT_UNSET(BYTE, POS) (BYTE &= ~(1<<POS))
#define BIT_WRITE(BYTE, POS, VAL) (BYTE = ((BYTE&(~(1<<POS))) | ((VAL)<<POS)))
#define NOP __asm__ __volatile__ ("nop\n\t")
/*
#define CLK 2
#define DATA 3
#define LS_A 4
#define LS_B 5
#define LS_C 6
#define LS_D 7
*/

#define CLK PD2
#define DATA PD3
#define LS_A PD4
#define LS_B PD5
#define LS_C PD6
#define LS_D PD7


uint8_t buffer[BUFFER_SIZE];
uint8_t hidden_buffer[BUFFER_SIZE];
uint8_t *buffer_ptr=buffer;
uint8_t *hidden_buffer_ptr=hidden_buffer;
uint8_t *selected_line=buffer;


static const uint8_t line_lookup_table[7]={4,2,6,1,5,3,7};

static void wait_cycles(uint8_t cycle){
  while(cycle-->0){
    NOP;
  }
}

static void switchBuffers(){
  uint8_t *tmp=buffer_ptr;
  buffer_ptr=hidden_buffer_ptr;
  hidden_buffer_ptr=buffer_ptr;
}

static void lineOff(){
  BIT_UNSET(PORTD,LS_A);
  BIT_UNSET(PORTD,LS_B);
  BIT_UNSET(PORTD,LS_C);
  BIT_UNSET(PORTD,LS_D);
}

static void selectLine(uint8_t A){
  A=line_lookup_table[A];
//  PORTD|=A<<4;
  BIT_SET_3(PORTD, LS_A, BIT(A, 0));
  BIT_SET_3(PORTD, LS_B, BIT(A, 1));
  BIT_SET_3(PORTD, LS_C, BIT(A, 2));
  BIT_SET_3(PORTD, LS_D, BIT(A, 3));
}

void switchLine(uint8_t A){
  selected_line=&buffer_ptr[(A+1)*LINE_BYTES-1];
}

static void clock(){
  BIT_SET(PORTD, CLK);
  NOP;
  BIT_UNSET(PORTD, CLK);
}

void writeLine(){
  uint8_t cnt;
  uint8_t *ptr=selected_line;
  uint8_t val;
  for(cnt=LINE_BYTES*8; cnt>0; cnt--){
    if(cnt%8 == 0){ //cnt%8==0
      val=*ptr--;
    }
    BIT_WRITE(PORTD, DATA, (val&1));
    clock();
    val>>=1;
  }
}

byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};

unsigned int localPort = 8888;      // local port to listen on

 EthernetUDP Udp;
 

void setup(){
  Ethernet.begin(mac);
  Udp.begin(localPort);
  BIT_SET(DDRD, CLK);
  BIT_SET(DDRD, DATA);
  BIT_SET(DDRD, LS_A);
  BIT_SET(DDRD, LS_B);
  BIT_SET(DDRD, LS_C);
  BIT_SET(DDRD, LS_D);
  uint16_t i=0;
  for(i=0; i<BUFFER_SIZE; i++){
    buffer[i]=hidden_buffer[i]=0; 
  }
}

void loop(){
  uint8_t i;
  int packetSize=0;
  for(i=0; i<7; i++){
     switchLine(i);
     lineOff();
     writeLine();
     selectLine(i);
     wait_cycles(0xFF);
     wait_cycles(0xFF);
     wait_cycles(0xFF);
     wait_cycles(0xFF);
     wait_cycles(0xFF);
     wait_cycles(0xFF);
     wait_cycles(0xFF);
  }
  lineOff();
  packetSize = Udp.parsePacket();
  if(packetSize){
   lineOff();
   Udp.read(hidden_buffer_ptr, BUFFER_SIZE); 
   switchBuffers();
  }
  Ethernet.maintain();
}
